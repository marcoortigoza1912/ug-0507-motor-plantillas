
const integrantes = [
  {nombre:"Marco", apellido:"Ortigoza", matricula: "UG0507",},
  {nombre: "Elvio",apellido: "Martinez", matricula: "Y12858"},
  {nombre: "Alexis",apellido:"Duarte", matricula: "Y17825"},
  {nombre: "Gabriel",apellido: "Garcete", matricula: "Y23865"},
  {nombre: "Lennys", apellido: "Cantero", matricula: "Y26426"}, 

]
const tipomedia = [
{nombre: "Imagen"},
{nombre: "Youtube"},
{nombre: "Dibujo" },
]

const media = [
{nombre: "Youtube", src:null, url:"https://www.youtube.com/embed/b8-tXG8KrWs?si=qBWFL28iK9JTU3JZ", matricula: "Y26426", titulo: "Video favorito de youtube" },
{nombre: "imagen", url:null, src:"/assets/images/imagen-56.jpeg", matricula: "Y26426", titulo:'Imagen representativa' },
{nombre: "dibujo",url:null, src:"../../assets/images/imagen-57.png", matricula: "Y26426", titulo:'Dibujo en Paint'  },

{nombre: "Youtube", src: null, url:"https://www.youtube.com/embed/RW75cGvO5xY", matricula:"UG0507", titulo: "Video favorito de youtube"},
{nombre: "Imagen", src: null, url: "/assets/images/saturno.jpg", matricula: "UG0507", titulo: "Imagen representativa"},
{nombre: "Dibujo", src: null, url:"../../assets/images/TERERE.png",matricula:"UG0507", titulo:"Dibujo en paint"},

{nombre: "Youtube", src: null, url:"https://www.youtube.com/embed/VhoHnKuf-HI", matricula:"Y12858", titulo: "Video favorito de youtube"},
{nombre: "Imagen", src: null, url: "../../assets/images/melissa.jpg", matricula: "Y12858", titulo: "Imagen representativa"},
{nombre: "Dibujo", src: null, url:"../../assets/images/dibujo-Elvio.png",matricula:"Y12858", titulo:"Dibujo en paint"},

{nombre: "Youtube", src: null, url:"https://www.youtube.com/embed/U1ivmi3_IeI?si=9QjM1bJzS3uUpMYk", matricula:"Y17825", titulo: "Video favorito de youtube"},
{nombre: "Imagen", src: null, url: "../../assets/images/imagen-personalidad.jpg", matricula: "Y17825", titulo: "Imagen representativa"},
{nombre: "Dibujo", src: null, url:"../../assets/images/dibujo-sistema-solar.png",matricula:"Y17825", titulo:"Dibujo en paint"},

{nombre: "Youtube", src: null, url:"https://www.youtube.com/embed/B4LvDiIi128?rel=0", matricula:"Y23865", titulo: "Video favorito de youtube"},
{nombre: "Imagen", src: null, url: "../../assets/images/messi_pou.jpeg", matricula: "Y23865", titulo: "Imagen representativa"},
{nombre: "Dibujo", src: null, url:"../../assets/images/paint_garcete.jpg",matricula:"Y23865", titulo:"Dibujo en paint"},

//Transformar el home
/*{
  url:"/",
  matricula:"Home",
  titulo: "Bienvenidos al Grupo XYZ",
  titulo2:"Bienvenidos al Grupo FSOCIETY",
  //logo: "Logo Del grupo",
  imagen:"./assets/images/logo.jpeg",
  //video:"",
  alt:"Grupo FSOCIETY",
  html: ''
}*/
]

const home = [
  { 
  nombre: 'FSOCIETY', 
  titulo: 'Bienvenidos al grupo', 
  src: '/assets/images/logo.jpeg', 
  alt: 'Grupo FSOCIETY' 
  }
]
exports.integrantes=integrantes;
exports.tipomedia = tipomedia;
exports.media = media;

exports.home=home;

