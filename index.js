const express = require('express');
const db = require("./db/data")
const hbs = require("hbs");
const app = express();

//conf para app express
app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

hbs.registerPartials(__dirname + "/views/partials");

//correccion de rutas
//Inicio 
app.get("/", (request, response) => {
    response.render("index", {
        home: db.home[0]
    });
});
//inicio
/*app.get("/index.html", (request, response) => {
    response.render("index", {
        home: db.home[0]
    });
});*/

app.get("/:matricula", (request, response, next) => {
    const matricula = request.params.matricula;

    // Buscar el integrante por la matrícula
    const integranteFilter = db.integrantes.find(integrante => integrante.matricula === matricula);

    // Filtrar los medios por la matrícula
    const medios = db.media.filter(media => media.matricula === matricula);

    // Verificar si se encontró el integrante o los medios
    if (!integranteFilter || medios.length === 0) {
        //renderizar la pagina "error" si el integrante es incorrecto
        response.render("error");
    }else{
        // Renderizar la página 'integrante' con los datos filtrados
    response.render('integrante', {
        integrante: integranteFilter,
        media: medios,
        tipomedia: db.tipomedia
    }); 
    }

    
});

/******************************************** */
app.get("/paginas/word_cloud.html", (request, response) => {
    response.render("word_cloud");
});
app.get("/paginas/curso.html", (request, response) => {
    response.render("curso");
});
app.listen(3000, () => {
    console.log("El servidor se está ejecutando en http://localhost:3000");
});










